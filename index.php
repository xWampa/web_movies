<!DOCTYPE html>
<?php include_once 'funciones_generales_php.php';?>
<?php include_once 'user.php';?>
<?php include_once 'user_session.php';?>
<?php echo TodaLaPescaDeSesion();?>

<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minumum-scale=1.0">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/custom.css">
		<title>Pewelilucas</title>	
	</head>

	<body class="bg-dark">
		<?php
		if(isset($_SESSION['user'])){
			echo ImprimirMenuA();
		}else{
			echo ImprimirMenuNA();
		}
		
		if (isset($_GET['orderby'])){
		}
		?>
		<div class="container">
			<div class="row">
				<?php 
					if(!isset($_GET['page'])){
						//Si no está inicializada
						$pagen = 1;
					}else{
						$pagen = $_GET['page'];
					}
				
					if(!isset($_POST['orderby'])){
						//Si no está inicializada
						if(!isset($_GET['orderby'])){
							$ordern = 0;
						}else{
						$ordern = $_GET['orderby'];
						}
					}else{
						$ordern = $_POST['orderby'];
						$_SESSION['orderby']=$ordern;
						
					}
					echo Pagination($pagen,$ordern);
				?>
				
			</div>
		</div>
		
		<button onclick="topFunction()" class="btn btn-primary" id=myBtn title="Go to top">Top</button>
		<br>
		<h3 class="text-light">
		</h3>
		<script>
			//Get the button
			var mybutton = document.getElementById("myBtn");

			// When the user scrolls down 20px from the top of the document, show the button
			window.onscroll = function() {scrollFunction()};

			function scrollFunction() {
			  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
				mybutton.style.display = "block";
			  } else {
				mybutton.style.display = "none";
			  }
			}

			// When the user clicks on the button, scroll to the top of the document
			function topFunction() {
			  document.body.scrollTop = 0;
			  document.documentElement.scrollTop = 0;
			}
		</script>
		<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

		<script src="js/popper.min.js"></script>

	</body>



</html>