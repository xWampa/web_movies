<!DOCTYPE html>
<?php include_once 'funciones_generales_php.php';?>
<?php include_once 'user.php';?>
<?php include_once 'user_session.php';?>
<?php
session_start();
//$movieid = $_GET['idm'];
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minumum-scale=1.0">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/custom.css">
		<title>Pewelilucas</title>	
	</head>
	<body class="bg-dark">
		<?php
		if(isset($_SESSION['user'])){
			echo ImprimirMenuA();
			echo $_SESSION['user'];//USALO PARA BUSQUEDA DE BBDD
		}else{
			echo ImprimirMenuNA();
		}?>
		<div class="container">
			<div class="row">
				<?php 
					if(!isset($_GET['page'])){
						//Si no está inicializada
						$pagen = 1;
					}else{
						$pagen = $_GET['page'];
					}
					if(!isset($_GET['genre'])){
						//Si no está inicializada
						$genre = 1;
						}else{
							$genre = $_GET['genre'];
						}
				
					if(!isset($_POST['orderby'])){
						//Si no está inicializada
						if(!isset($_SESSION['orderby'])){
							$ordern = 0;
						}else{
						$ordern = $_SESSION['orderby'];
						}
					}else{
						$ordern = $_POST['orderby'];
						$_SESSION['orderby']=$ordern;
						
					}
					echo ImprimeSearchv2genre($pagen,$genre,$ordern);
				?>
			</div>
		</div>
		<br>
		<h3 class="text-light">
		</h3>
				<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/popper.min.js"></script>
		</body>
</html>