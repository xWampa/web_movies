<!DOCTYPE html>
<?php include('funciones_generales_php.php');?>
<?php include_once 'user.php';?>
<?php include_once 'user_session.php';?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minumum-scale=1.0">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/custom.css">
		
		<title>My account</title>	
	</head>
	<!--#################################################################################################################################################-->
	<body>
		<?php echo ImprimirMenuA(); 
			session_start(); //Para conseguir la sesion de otras paginas
			if(isset($_SESSION['user'])) {
	  			echo "Your session is running " . $_SESSION['user'];
			}
		?>
		<div class="container">
    <div class="row my-2">
        <div class="col-lg-8 order-lg-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#messages" data-toggle="tab" class="nav-link">Opinions</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Edit profile</a>
                </li>
            </ul>
            <div class="tab-content py-4">
                <div class="tab-pane active" id="profile">
                    <h5 class="mb-3">User Profile</h5>
                    <div class="row">
                        <?php echo ImprimeDetallesUser($_SESSION['user']);?>
                       
                        
                    </div>
                    <!--/row-->
                </div>
				<!--/MAYBE TUS ULTIMOS COMENTARIO A PELICULAS-->
                <div class="tab-pane" id="messages">
                    
                    <table class="table table-hover table-striped">
                        <tbody>                                    
                            <?php echo ImprimeOpinionesUser($_SESSION['user']);?>                                    
                            <!--tr>
                                <td>
                                   <span class="float-right font-weight-bold">3 hrs ago</span> Here is your a link to the latest summary report from the..
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="float-right font-weight-bold">Yesterday</span> There has been a request on your account since that was..
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="float-right font-weight-bold">9/10</span> Porttitor vitae ultrices quis, dapibus id dolor. Morbi venenatis lacinia rhoncus. 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="float-right font-weight-bold">9/4</span> Vestibulum tincidunt ullamcorper eros eget luctus. 
                                </td>
                            </tr>
                            <tr>
                                <td>
                                   <span class="float-right font-weight-bold">9/4</span> Maxamillion ais the fix for tibulum tincidunt ullamcorper eros. 
                                </td>
                            </tr-->
                        </tbody> 
                    </table>
                </div>
                <div class="tab-pane" id="edit">
                    <form role="form" action="profile_edited.php" method="POST" enctype="multipart/form-data">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Name</label>
                            <div class="col-lg-9">
                                <input name="name" type="name" id="inputName" class="form-control" required="" autofocus=""><!--/NOMBER-->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Age</label>
                            <div class="col-lg-9">
                                <input name="age" type="age" id="inputAge" class="form-control" required="" autofocus=""><!--/Edad-->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Email</label>
                            <div class="col-lg-9">
                                <input name="email" type="email" id="inputEmail" class="form-control" required="" autofocus=""><!--/EMAIL-->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Sex</label>
                            <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="M" required="" autofocus="">
				  <label class="form-check-label" for="inlineRadio2">Male</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="F">
				  <label class="form-check-label" for="inlineRadio1">Female</label>
                            </div>
							<div class="form-check form-check">
							  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" disabled>
							  <label class="form-check-label" for="inlineRadio3">Apache helicopter</label>
							</div>
                        </div>
                        <div class="form-group row">
						   <select class="form-control" name="ocupacion" required="" autofocus="">
							   <option value="">Select occupation</option> 
							   <option value="administrator">administrator</option> 
							   <option value="artist">artist</option> 
							   <option value="doctor">doctor</option>
							   <option value="educator">educator</option> 
							   <option value="engineer">engineer</option> 
							   <option value="entertainment">entertainment</option> 
							   <option value="executive">executive</option> 
							   <option value="healthcare">healthcare</option> 
							   <option value="homemaker">homemaker</option> 
							   <option value="lawyer">lawyer</option> 
							   <option value="librarian">librarian</option> 
							   <option value="marketing">marketing</option> 
							   <option value="none">none</option> 
							   <option value="other">other</option> 
							   <option value="programmer">programmer</option> 
							   <option value="retire">retire</option> 
							   <option value="salesman">salesman</option> 
							   <option value="scientist">scientist</option> 
							   <option value="student">student</option> 
							   <option value="technician">technician</option> 
							   <option value="writer">writer</option>
							</select>
                        </div>
                       
                      
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Password</label>
                            <div class="col-lg-9">
                                <input name="passwd" type="password" id="inputPassword" class="form-control" placeholder="Password" required=""><!--/PW-->
                            </div>
                        </div>
                        
						
							<!-- IMAGE -->
						<div class="input-group">
						  <div class="input-group-prepend">
							<span class="input-group-text" id="inputGroupFileAddon01">Png only | Profile Pic</span>
						  </div>
						  <div class="custom-file">
							<input name="pic" type="file" class="custom-file-input" id="inputGroupFile01"
							  aria-describedby="inputGroupFileAddon01" accept="image/png" required="">
							<label class="custom-file-label" for="inputGroupFile01"></label>
							  <input type="hidden" name="parabd" value="<?php $_SESSION['user'];?>">
						  </div>
						</div>
						<br>
						<button name="submit" class="btn btn-lg btn-primary btn-block" type="submit">Save changes</button>
							
                    </form>
					<!--/aqui-->
					
						<!--COSAS DEL VIDEO TUTORIAL-->
					<div id="todolist">
						<?php 

							//if(isset($_POST['submit'])){
							//	$name = $_POST['name'];
								//$mail = $_POST['email'];
								//$edad = $_POST['age'];
								//$sex = $_POST['inlineRadioOptions'];
							//	$ocupacion = $_POST['ocupacion'];
							//	$pic = basename($_FILES['pic']['name']);
							//	$pico = $_FILES['pic']['tmp_name'];
							//	$passwd = sha1($_POST['passwd']);
							//	echo EditaUsuario($name,$mail,$edad,$sex,$ocupacion,$pic,$passwd,$pico,$_SESSION['user']);
								
							//	header("location:logout.php");
							//}

						?>
						</div>
            </div>
        </div>
		</div>
        <div class="col-lg-4 order-lg-1 text-center">
			<?php echo ImprimeImagenPerfil($_SESSION['user']);?>
            <!--img src="//placehold.it/150" class="mx-auto img-fluid img-circle d-block" alt="avatar"-->
            
        </div>
    </div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/popper.min.js"></script>
		</body>
</html>