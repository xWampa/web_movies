<!DOCTYPE html>
<?php include_once 'funciones_generales_php.php';?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minumum-scale=1.0">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/custom.css">
		
		<title>Resgister</title>	
	</head>
		<body class="text-center" style="width: 300">
			<form class="form-signin" action="" method="POST" enctype="multipart/form-data">
				<a href="index.php">
			  		<img class="mb-4" src="img/LogoTLP.png" alt="" width="72" height="72" >
				</a>	
			  <h1 class="h3 mb-3 font-weight-normal">Register form</h1>
				<!-- NAME -->
			  <label for="inputName" class="sr-only">Name</label>
			  <input name="name" type="name" id="inputName" class="form-control" placeholder="Name" required="" autofocus="">	
			  	<!-- Email -->
			  <label for="inputEmail" class="sr-only">Email address</label>
			  <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
					<!-- Age -->
			  <label for="inputAge" class="sr-only">Age</label>
			  <input name="age" type="age" id="inputAge" class="form-control" placeholder="Age" required="" autofocus="">
				<!-- radios -->

				<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="M" required="" autofocus="">
				  <label class="form-check-label" for="inlineRadio1">Male</label>
				</div>
				<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="F">
				  <label class="form-check-label" for="inlineRadio2">Female</label>
				</div>
				<div class="form-check form-check">
				  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3" disabled>
				  <label class="form-check-label" for="inlineRadio3">Apache helicopter</label>
				</div>
				<!-- Ocupacion >
				<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio1" value="option1">
				  <label class="form-check-label" for="inlineRadio4">Student</label>
				</div>
				<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="inlineRadioOptions2" id="inlineRadio2" value="option2">
				  <label class="form-check-label" for="inlineRadio5">Teacher</label>
				</div-->
				<select class="form-control" name="ocupacion" required="" autofocus="">
				   <option value="">Select occupation</option> 
				   <option value="administrator">administrator</option> 
				   <option value="artist">artist</option> 
				   <option value="doctor">doctor</option>
				   <option value="educator">educator</option> 
				   <option value="engineer">engineer</option> 
				   <option value="entertainment">entertainment</option> 
				   <option value="executive">executive</option> 
				   <option value="healthcare">healthcare</option> 
				   <option value="homemaker">homemaker</option> 
				   <option value="lawyer">lawyer</option> 
				   <option value="librarian">librarian</option> 
				   <option value="marketing">marketing</option> 
				   <option value="none">none</option> 
				   <option value="other">other</option> 
				   <option value="programmer">programmer</option> 
				   <option value="retire">retire</option> 
				   <option value="salesman">salesman</option> 
				   <option value="scientist">scientist</option> 
				   <option value="student">student</option> 
				   <option value="technician">technician</option> 
				   <option value="writer">writer</option>
				</select>
				
				
			  <label for="inputPassword" class="sr-only">Password</label>
			  <input name="passwd" type="password" id="inputPassword" class="form-control" placeholder="Password" required="">
				
				<!-- IMAGE -->
				<div class="input-group">
  <div class="input-group-prepend">
    <span class="input-group-text" id="inputGroupFileAddon01">Png only | Profile Pic</span>
  </div>
  <div class="custom-file">
    <input name="pic" type="file" class="custom-file-input" id="inputGroupFile01"
      aria-describedby="inputGroupFileAddon01" accept="image/png" required="">
    <label class="custom-file-label" for="inputGroupFile01"></label>
  </div>
</div>
				
				
			  <div class="checkbox mb-3">
				<label>
				  <input type="checkbox" value="remember-me"> Remember me
				</label>
			  </div>
			  <button name="submit" class="btn btn-lg btn-primary btn-block" type="submit">Save changes</button>
			  <a class="mt-5 mb-3 text-muted" href="login.php">Already registered? Login now!</a>
			</form>
			
			<!--COSAS DEL VIDEO TUTORIAL-->
			<div id="todolist">
				<?php 

					if(isset($_POST['submit'])){
						$name = $_POST['name'];
						$mail = $_POST['email'];
						$edad = $_POST['age'];
						$sex = $_POST['inlineRadioOptions'];
						$ocupacion = $_POST['ocupacion'];
						$pic = basename($_FILES['pic']['name']);
						$pico = $_FILES['pic']['tmp_name']; //nombre random del archivo que se encuentra en xampp/tmp/
						$passwd = sha1($_POST['passwd']);
						echo RegistraUsuario($name,$mail,$edad,$sex,$ocupacion,$pic,$passwd,$pico);
					}

				?>
			</div>

			<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/popper.min.js"></script>
		</body>
</html>