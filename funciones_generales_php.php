<?php
function ImprimirMenuNA() {
	$devolver = '';
	$devolver .= '<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <img alt="Teleco LAN Party" width="80" height="60" data-sticky-width="154" data-sticky-height="45" data-sticky-top="33" src="img/LogoTLP.png">
		  <a class="navbar-brand" href="index.php">Peweliculas</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					  <li class="nav-item active">
						<a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
					  </li>
					  <li class="nav-item dropdown">
					  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					  Order by
					  </a>
					  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					  <form action="" method="POST">
						<button class="btn btn-link" name="orderby" type="submit" value="name">Name</button>
						  <div class="dropdown-divider"></div>
						<button class="btn btn-link" name="orderby" type="submit" value="rate">Rate</button>
					  </form>
					  </li>
					  <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  By genre
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						  <a class="dropdown-item" href="genre.php?genre=1">Action</a>
						  <a class="dropdown-item" href="genre.php?genre=2">Adventure</a>
						  <a class="dropdown-item" href="genre.php?genre=3">Animation</a>
						  <a class="dropdown-item" href="genre.php?genre=4">For children</a>
						  <a class="dropdown-item" href="genre.php?genre=5">Comedy</a>
						  <a class="dropdown-item" href="genre.php?genre=6">Crime</a>
						  <a class="dropdown-item" href="genre.php?genre=7">Documentary</a>
						  <a class="dropdown-item" href="genre.php?genre=8">Drama</a>
						  <a class="dropdown-item" href="genre.php?genre=9">Fantasy</a>
						  <a class="dropdown-item" href="genre.php?genre=10">Film-Noir</a>
						  <a class="dropdown-item" href="genre.php?genre=11">Horror</a>
						  <a class="dropdown-item" href="genre.php?genre=12">Musical</a>
						  <a class="dropdown-item" href="genre.php?genre=13">Mystery</a>
						  <a class="dropdown-item" href="genre.php?genre=14">Romance</a>
						  <a class="dropdown-item" href="genre.php?genre=15">Sci-Fi</a>
						  <a class="dropdown-item" href="genre.php?genre=16">Thriller</a>
						  <a class="dropdown-item" href="genre.php?genre=17">War</a>
						  <a class="dropdown-item" href="genre.php?genre=18">Western</a>
						  <div class="dropdown-divider"></div>
						  <a class="dropdown-item" href="#">Something else here</a>
						</div>
					  </li>
					  <li class="nav-item">
						<a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
					  </li>
					<li class ="nav-item">
						<a class="nav-link" href="login.php">Login</a>
					</li>
					<li class ="nav-item active btn-primary btn-sm">
						<a class="nav-link" href="register.php">Register</a>
					</li>

				</ul>
			<form class="form-inline my-2 my-lg-0" action="search.php" method="GET">
			  <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search" aria-label="Search">
			  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
		  </div>
		</nav> ';
	return $devolver;
}	

function ImprimirMenuA() {
	$devolver = '';
	$devolver .= '<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <img alt="Teleco LAN Party" width="80" height="60" data-sticky-width="154" data-sticky-height="45" data-sticky-top="33" src="img/LogoTLP.png">
		  <a class="navbar-brand" href="index.php">Peweliculas</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					  <li class="nav-item active">
						<a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a>
					  </li>
					  <li class="nav-item dropdown">
					  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					  Order by
					  </a>
					  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					  <form action="" method="GET">
						<button class="btn btn-link" name="orderby" type="submit" value="name">Name</button>
						  <div class="dropdown-divider"></div>
						<button class="btn btn-link" name="orderby" type="submit" value="rate">Rate</button>
					  </form>
					  </li>
					  <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						  By genre
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						  <a class="dropdown-item" href="genre.php?genre=1">Action</a>
						  <a class="dropdown-item" href="genre.php?genre=2">Adventure</a>
						  <a class="dropdown-item" href="genre.php?genre=3">Animation</a>
						  <a class="dropdown-item" href="genre.php?genre=4">For children</a>
						  <a class="dropdown-item" href="genre.php?genre=5">Comedy</a>
						  <a class="dropdown-item" href="genre.php?genre=6">Crime</a>
						  <a class="dropdown-item" href="genre.php?genre=7">Documentary</a>
						  <a class="dropdown-item" href="genre.php?genre=8">Drama</a>
						  <a class="dropdown-item" href="genre.php?genre=9">Fantasy</a>
						  <a class="dropdown-item" href="genre.php?genre=10">Film-Noir</a>
						  <a class="dropdown-item" href="genre.php?genre=11">Horror</a>
						  <a class="dropdown-item" href="genre.php?genre=12">Musical</a>
						  <a class="dropdown-item" href="genre.php?genre=13">Mystery</a>
						  <a class="dropdown-item" href="genre.php?genre=14">Romance</a>
						  <a class="dropdown-item" href="genre.php?genre=15">Sci-Fi</a>
						  <a class="dropdown-item" href="genre.php?genre=16">Thriller</a>
						  <a class="dropdown-item" href="genre.php?genre=17">War</a>
						  <a class="dropdown-item" href="genre.php?genre=18">Western</a>
						  <div class="dropdown-divider"></div>
						  <a class="dropdown-item" href="#">Something else here</a>
						</div>
					  </li>
					  <li class="nav-item">
						<a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
					  </li>
					<li class ="nav-item">
						<a class="nav-link" href="logout.php">Logout</a>
					</li>
					<li class ="nav-item active btn-primary btn-sm">
						<a class="nav-link" href="myaccount.php">My account</a>
					</li>

				</ul>
			<form class="form-inline my-2 my-lg-0" action="search.php" method="GET">
			  <input class="form-control mr-sm-2" type="search" name="search" placeholder="Search" aria-label="Search">
			  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
			</form>
		  </div>
		</nav> ';
	return $devolver;
}	

function testDB(){
	$devolver='';
	if (mysql_connect ("localhost", "root", "") ){
		echo "<p>MySQL le ha dado permiso a PHP para ejecutar consultas con ese usuario y clave</p>";
		}else{
		echo "<p>MySQL no conoce ese usuario y password, y rechaza el intento de conexión</p>";
			}
}
//IMPRIME EL ID DE LOS GENEROS QUE HAY POR PANTALLA
function simpleDB(){
	 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
   $sql = mysqli_query($connection, "SELECT * FROM genre");
   while($row = mysqli_fetch_array($sql)) {
   	$names[] = $row['id'];
   }
	foreach($names as $name) {
		echo "$name";
	}
}

function ImprimirPic(){
		 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
	
	//onerror="this.onerror=null; this.src='Default.jpg'"
	$defRoute	 = "'img/mov/default.jpg'";
	$parteMedio	 = "this.onerror=null; this.src=";
	$parteInicio = 'onerror="';
	$parteFinal  = '"';	
	//$parteInicio.$parteMedio.$defRoute.$parteFinal
	
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
   $sql = mysqli_query($connection, "SELECT id,title, url_pic, url_imdb FROM movie LIMIT 0,20");
	while($row = mysqli_fetch_array($sql)){
		$resultado[] = $row;
	}
	foreach($resultado as $consulta){
		echo ' 
		<div class="col-md-2">
			<a href="details.php?id='.$consulta['id'].'">
					<img src="img/mov/'.$consulta['url_pic'].'" '.$parteInicio.$parteMedio.$defRoute.$parteFinal.'class="img-fluid margenbot" alt="img/mov/1995.jpg">
				</a>
			<div class="crop">
						<a href="https://repelis.me/05/star-wars-episodio-ix-el-ascenso-de-skywalker-2019/" class="info-title one-line">
							<h6 class="crop">'.$consulta['title'].'</h6>
						</a>
					</div>
				</div>
		
		
		
		
		';
	}
}
//DEVUELVE CATALOGO DE PELICULAS DE ACUERDO CON EL $search PROPORCIONADO
function ImprimirSearch($search,$getpage){
		 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
	
	//onerror="this.onerror=null; this.src='Default.jpg'"
	$defRoute	 = "'img/mov/default.jpg'";
	$parteMedio	 = "this.onerror=null; this.src=";
	$parteInicio = 'onerror="';
	$parteFinal  = '"';	
	//$parteInicio.$parteMedio.$defRoute.$parteFinal
	
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
   $sql = mysqli_query($connection, "SELECT id,title, url_pic, url_imdb FROM movie WHERE title LIKE '%$search%'");
	//guarda el numero de filas que devuleve la consulta
	$queryResult = mysqli_num_rows($sql);
	//si tiene mas de 0 filas entonces:
	if($queryResult > 0){
		$results_per_page = 24;
		if(isset($getpage)){
			$page = $getpage;
		}else {
			$page = 1;
		}
		while($row = $sql->fetch_assoc()){
		echo ' 
		<div class="col-md-2">
			<a href="details.php?id='.$row['id'].'">
					<img src="img/mov/'.$row['url_pic'].'" '.$parteInicio.$parteMedio.$defRoute.$parteFinal.'class="img-fluid margenbot" alt="img/mov/1995.jpg">
				</a>
			<div class="crop">
						<a href="https://repelis.me/05/star-wars-episodio-ix-el-ascenso-de-skywalker-2019/" class="info-title one-line">
							<h6 class="crop">'.$row['title'].'</h6>
						</a>
					</div>
				</div>
		';
		$sql = "SELECT COUNT(id) AS total FROM movie"; 
		$result = $connection->query($sql);
		$row = $result->fetch_assoc();
		$total_pages = ceil($row["total"] / $results_per_page);

		//AQUI TENGO LOS HREF PARA CADA PAGINA
		$previous = $page -1;
		$next	  = $page +1;

		if(!isset($page) || $page==1){
			echo '
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="index.php?page=2"><strong>Next</strong></a></li>
			</ul>
			</div>';
		}else if($page == $total_pages){
			echo'
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="index.php?page='.$previous.'"><strong>Previous</strong></a></li>
			</ul>
			</div>';
		}else {
			echo'
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="index.php?page='.$previous.'"><strong>Previous</strong></a></li>
			  <li class="page-item"><a class="page-link" href="index.php?page='.$next.'"><strong>Next</strong></a></li>
			</ul>
			</div>';
		} 
	}
	//No se han producido resultados
	}else{
		echo 'There are no results matching your search :( <br>
		Your search was: '.$search;
	}
	
}
function ImprimeDetalles($id){	
		 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
   $sql = mysqli_query($connection, "SELECT *,(SELECT AVG(score) as avg FROM user_score WHERE id_movie=$id) as rating,(SELECT COUNT(*) FROM user_score WHERE id_movie=1 GROUP BY id_movie) AS nrates FROM movie WHERE id=$id");
	while($row = mysqli_fetch_array($sql)){
		$resultado[] = $row;
	}
	foreach($resultado as $consulta){
		echo ' 
		<h5 class="mb-3">'.$consulta['title'].'</h5>
                    <div class="row">
                        <div class="col-md-6">
                           
                            <h6>Description</h6>
                            <p>
                                '.$consulta['desc'].'
                            </p>
							<h6>Release date</h6>
                            <p>
                                '.$consulta['date'].'
                            </p>
							<h6>Number of times rated</h6>
							<p>
								'.$consulta['nrates'].'
							</p>
							<h6>Rating</h6>
                            <p>
                                '.$consulta['rating'].'	/5							
                            </p>
                        </div>
                       
                        
                    </div>
		
		';
	}
}
function ImprimeGenres($id){	
		 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
   $sql = "SELECT id,name FROM genre WHERE id IN(SELECT genre FROM moviegenre WHERE movie_id=$id)";
	$rs_result = $connection->query($sql);
	while($row = $rs_result->fetch_assoc()){
		$resultado[] = $row;
		echo ' 
			<a href="genre.php?genre='.$row['id'].'" class="badge badge-dark badge-pill">'.$row['name'].'</a>
		
		';
	}
		
}
//IMPRIME TODAS LAS OPINIONES QUE EL USUARIO HA HECHO
function ImprimeDetallesUser($user){	
		 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
   $sql = mysqli_query($connection, "SELECT * FROM users WHERE name='$user'");
	while($row = mysqli_fetch_array($sql)){
		echo ' 
		<div class="col-md-6">
                            <h6>Name</h6>
                            <p>
                                '.$row['name'].'
                            </p>
                            <h6>Age</h6>
                            <p>
                                '.$row['edad'].'
                            </p>
							<h6>Sex</h6>
                            <p>
                                '.$row['sex'].'
                            </p>
							<h6>Occupation</h6>
                            <p>
                                '.$row['ocupacion'].'
                            </p>
                        </div>
		
		';
	}
		
	
}
function ImprimeOpiniones($id){
	
		 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
		$sql = mysqli_query($connection, "SELECT movie_id,comment,user_id,(SELECT name FROM users WHERE id=user_id) AS name FROM moviecomments WHERE movie_id=$id");
		while($row = mysqli_fetch_array($sql)){
		$resultado[] = $row;
	}
		if(!empty($resultado)){
			foreach($resultado as $consulta){
			echo '
								<tr>
									<td>
									   <span class="float-right font-weight-bold">'.$consulta['name'].'</span>'.$consulta['comment'].'
									</td>
								</tr>
			';
			}
		}
}
function ImprimeOpinionesUser($user){
	
		 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
		$sql = mysqli_query($connection, "SELECT movie_id,comment,(SELECT title FROM movie WHERE id=movie_id) AS peli FROM moviecomments WHERE user_id=(SELECT id FROM users WHERE name='$user')");
		while($row = mysqli_fetch_array($sql)){
		$resultado[] = $row;
	}
	if(!empty($resultado)){
		foreach($resultado as $consulta){
		echo '
							<tr>
                                <td>
                                   <a href="details.php?id='.$consulta['movie_id'].'"><span class="float-right font-weight-bold">'.$consulta['peli'].'</span></a>'.$consulta['comment'].'
                                </td>
                            </tr>
		';
		}
	}else{
		echo'<div class="alert alert-info alert-dismissable">
							<a class="panel-close close" data-dismiss="alert">×</a> Hey there! You have not commented any film. We <strong>encourage</strong> you to start contributing.
						</div>';
	}
}
//IMPRIME LA IMAGEN DE LA PELICULA EN details.php
function ImprimeImagen($id){
		 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
	
	//onerror="this.onerror=null; this.src='Default.jpg'"
	$defRoute	 = "'img/mov/default.jpg'";
	$parteMedio	 = "this.onerror=null; this.src=";
	$parteInicio = 'onerror="';
	$parteFinal  = '"';	
	//$parteInicio.$parteMedio.$defRoute.$parteFinal
	
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
   $sql = mysqli_query($connection, "SELECT url_pic FROM movie WHERE id=$id");

	while ($row = mysqli_fetch_array($sql)) {
        echo '<img src="img/mov/'.$row['url_pic'].'" '.$parteInicio.$parteMedio.$defRoute.$parteFinal.'class=" rounded border border-primary border-3 mx-auto img-fluid img-circle d-block" alt="avatar">
		
		';
    }
}

function ImprimeImagenPerfil($user){
		 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
	
	//onerror="this.onerror=null; this.src='Default.jpg'"
	$defRoute	 = "'img/mov/default.jpg'";
	$parteMedio	 = "this.onerror=null; this.src=";
	$parteInicio = 'onerror="';
	$parteFinal  = '"';	
	//$parteInicio.$parteMedio.$defRoute.$parteFinal
	//desde el corchete hassta la c de clase
	//].'" '.$parteInicio.$parteMedio.$defRoute.$parteFinal.'c
	
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
   $sql = mysqli_query($connection, "SELECT pic FROM users WHERE name='$user'");

	while ($row = mysqli_fetch_array($sql)) {
        echo '<img src="img/usr/'.$row['pic'].'" class=" rounded border border-primary border-3 mx-auto img-fluid img-circle d-block" alt="avatar">
		
		';
    }
}
function ImprimeScore($user,$mov_id){
			 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
	
	$ratiodado = false;//Controlador si tiene la pelicula valorada
    //connect to mysqli database (Host/Username/Password)
    $connection = mysqli_connect($db_host, $db_username, $db_password) or die("Error " . mysqli_error());
    //select MySQLi dabatase table
    $db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
   $sql = mysqli_query($connection, "SELECT score FROM user_score WHERE id_user=(SELECT id FROM users WHERE name='$user') AND id_movie=$mov_id");

	while ($row = mysqli_fetch_array($sql)) {		
			echo '<h6><strong>Rate given: </strong>'.$row['score'].'</h6><br>';
		$ratiodado = true;//Si hace esta while es que tiene la peli valorada, ponemos el controlador a true
		}
		if($ratiodado == false){//si el controlador sigue en false es que no ha valorado la peli y mostramos el mensaje por pantalla
				echo '
				<div class="alert alert-warning alert-dismissable">
							<a class="panel-close close" data-dismiss="alert">×</a> You have <strong>not</strong> rated this film yet.
						</div>
				';
			}
}
function TodaLaPescaDeSesion(){
		$userSession = new UserSession();
	$user = new User();
	if(isset($_SESSION['user'])){
		//echo "hay sesion"; //CUANDO ABRIMOS NAVEGADOR Y NO HEMOS CERRADO SESION
		$user->setUser($userSession->getCurrentUser());
		include_once 'index.php';
	}else if(isset($_POST['username'])&& isset($_POST['password'])){
		//echo "Validacion de login";//EXITO EN LA VALIDACION DE USUAIRO
		$userForm = $_POST['username'];
		$passForm = $_POST['password'];

		if($user->userExist($userForm, $passForm)){
			echo "Usuario validado";
			$userSession->setCurrentUser($userForm);
			$user->setUser($userForm);

			header("location:index.php");
			//include_once 'index.php';
		}else{//ESTA SI FALLAS EN EL LOGIN
			//echo "nombre o pw fallida";
			$errorLogin = "Nombre de usuario y/o pw es incorrecto";
			include_once 'login.php';
		}
	}else{//ESTA ES SI NO ESTAS LOGUEADO ES LO QUE PASA
		//echo "Login";
		//include_once 'login.php';
	}
}
function RegistraUsuario($name,$mail,$edad,$sex,$ocupacion,$pic,$passwd,$pico){
					$db_host     = "localhost";
					$db_username = "root";
					$db_password = "";
					$db			 = "ai21";
					$statusMsg = '';
					$targetDir = "img/usr/";
					$fileName =  $pic;
					$targetFilePath = $targetDir . $fileName;
					$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
					$allowTypes = array('jpg','png','jpeg','gif','pdf');
						// Allow certain file formats
						if(in_array($fileType, $allowTypes)){
							// Upload file to server
							if(move_uploaded_file($pico, $targetFilePath)){
									// Insert image file name into database
									$statusMsg = "The file ".$fileName. " has been uploaded successfully.";

							}else{
								$statusMsg = 'Sorry';
							}

						//connect to mysqli database (Host/Username/Password/db)
						$connection = mysqli_connect($db_host, $db_username, $db_password);
						$db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
						if($connection ->connect_error){
							die("Conexion fallida: " . $connection->connect_error);
						}
							
							$sql=mysqli_query($connection,"SELECT * FROM users WHERE name='$name'");
							 if(mysqli_num_rows($sql)>=1)
							   {
								$statusMsg = "EL USUARIO YA EXISTE!";
								echo
								 '<h4 class="text-danger"><a class="mt-5 mb-3 text-muted text-danger" href="login.php">'.$statusMsg.'</a></h4>';
							   }
							 else
								{
							   //insert query goes here
							$sql = "INSERT INTO users(name,mail,edad,sex,ocupacion,pic,passwd) VALUES('$name','$mail','$edad','$sex','$ocupacion','$pic','$passwd')";
							if($connection->query($sql)==true){
								$statusMsg = "Registered! Now login please";
							}else{
								die("error al insertar datos". $connection->error);
							}
							echo '<h4><a class="mt-5 mb-3 text-muted" href="login.php">'.$statusMsg.'</a></h4>';
								}
							//antes iba aqui
							
							
							
							
							
							

						}
}

function EditaUsuario($name,$mail,$edad,$sex,$ocupacion,$pic,$passwd,$pico,$editaesta){
					$db_host     = "localhost";
					$db_username = "root";
					$db_password = "";
					$db			 = "ai21";
					$statusMsg = '';
					$targetDir = "img/usr/";
					$fileName = $pic;
					$targetFilePath = $targetDir . $fileName;
					$fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
					$allowTypes = array('jpg','png','jpeg','gif','pdf');
						// Allow certain file formats
						if(in_array($fileType, $allowTypes)){
							// Upload file to server
							if(move_uploaded_file($pico, $targetFilePath)){
									// Insert image file name into database
									$statusMsg = "The file ".$fileName. " has been uploaded successfully.";

							}else{
								$statusMsg = 'Sorry';
							}

						//connect to mysqli database (Host/Username/Password/db)
						$connection = mysqli_connect($db_host, $db_username, $db_password);
						$db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
						if($connection ->connect_error){
							die("Conexion fallida: " . $connection->connect_error);
						}							
							 
							   //insert query goes here
							$sql = "UPDATE users SET name='$name', mail='$mail', edad=$edad, sex='$sex', ocupacion='$ocupacion', pic='$pic', passwd='$passwd' WHERE name='$editaesta'";
							if($connection->query($sql)==true){
								$statusMsg = "CHANGES UPDATED SUCCESFULLY";
							}else{
								die("error al insertar datos". $connection->error);
							}
							echo '<h4><a class="mt-5 mb-3 text-muted" href="login.php">'.$statusMsg.'</a></h4>';
								
							//antes iba aqui
					}
}
//PARA INSERTAR COMENTARIO EN LA BD
function AnadeComentario($movie_id,$username,$comment){
			 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
	
	$ratiodado = false;//Controlador si tiene la pelicula valorada
    //connect to mysqli database (Host/Username/Password)
   $connection = mysqli_connect($db_host, $db_username, $db_password);
						$db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
						if($connection ->connect_error){
							die("Conexion fallida: " . $connection->connect_error);
						}//insert query goes here
							$sql = "INSERT INTO moviecomments (movie_id, user_id, comment) VALUES ('$movie_id', (SELECT id FROM users WHERE name='$username'), '$comment');";
							if($connection->query($sql)==true){
								$statusMsg = "Comentario añadido";
							}else{
								die("error al insertar datos". $connection->error);
							}
							//echo '<h4><a class="mt-5 mb-3 text-muted" href="index.php">'.$statusMsg.'</a></h4>';
}
//PARA EDITAR RATING DEL USUARIO A LA PELICULA
function AnadeRating($idm,$idu,$rating){
			 //MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
	
	$ratiodado = false;//Controlador si tiene la pelicula valorada
    //connect to mysqli database (Host/Username/Password)
   $connection = mysqli_connect($db_host, $db_username, $db_password);
						$db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
						if($connection ->connect_error){
							die("Conexion fallida: " . $connection->connect_error);
						}//insert query goes here
	
						$hoy = date("Y-m-d H:i:s");
						$sql=mysqli_query($connection,"SELECT * FROM user_score WHERE id_movie='$idm' AND id_user=(SELECT id FROM users WHERE name='$idu')");
							 if(mysqli_num_rows($sql)>=1)
							   {
								$sql = "UPDATE user_score SET score='$rating', time='$hoy' WHERE user_score.id_user=(SELECT id FROM users WHERE name='$idu') AND user_score.id_movie=$idm";
									 if($connection->query($sql)==true){
									$statusMsg = "Rating registered!";
									 }
								echo
								 '<h4 class="text-danger"><a class="mt-5 mb-3 text-muted text-danger" href="index.php">'.$statusMsg.'</a></h4>';
							   }
							 else
								{
							   //insert query goes here
							$sql = "INSERT INTO user_score(id_user,id_movie,score,time) VALUES((SELECT id FROM users WHERE name='$idu'),'$idm','$rating','$hoy')";
							if($connection->query($sql)==true){
								$statusMsg = "Rating registered!";
							}else{
								die("error al insertar datos". $connection->error);
							}
							echo '<h4><a class="mt-5 mb-3 text-muted" href="login.php">'.$statusMsg.'</a></h4>';
								}
	
}

function Pagination($getpage,$getorderby){
	//MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
	
	//onerror="this.onerror=null; this.src='Default.jpg'"
	$defRoute	 = "'img/mov/default.jpg'";
	$parteMedio	 = "this.onerror=null; this.src=";
	$parteInicio = 'onerror="';
	$parteFinal  = '"';	
	//$parteInicio.$parteMedio.$defRoute.$parteFinal
	
    //connect to mysqli database (Host/Username/Password)
   $connection = mysqli_connect($db_host, $db_username, $db_password);
	$db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
	if($connection ->connect_error){
		die("Conexion fallida: " . $connection->connect_error);
	}//insert query goes here
	$results_per_page = 24;
	if(isset($getpage)){
		$page = $getpage;
	}else {
		$page = 1;
	}
	$start_form = ($page-1) * $results_per_page;
	//Consulta para obtener todas las peliculas Limitando su echo a $resuls per page
	//Si no se ha especificado un orden

	if($getorderby == 'mierda'){
		$sql = "SELECT * FROM movie LIMIT $start_form,$results_per_page";
	
	}else if($getorderby == 'name'){
		$sql = "SELECT * FROM movie ORDER BY title LIMIT $start_form,$results_per_page";
	}else{
		$sql = "SELECT id_movie AS id,AVG(score)as avg,(SELECT title FROM movie WHERE id=id_movie) AS title,(SELECT url_pic FROM movie WHERE id=id_movie) AS url_pic FROM user_score GROUP BY id_movie ORDER BY avg DESC LIMIT $start_form,$results_per_page";
		
	}//Ejecucin de la consulta anterior
	$rs_result = $connection->query($sql);
	//Imprime los resultados (limitado a $result_per_page)
	while($row = $rs_result->fetch_assoc()){
		echo ' 
		<div class="col-md-2">
			<a href="details.php?id='.$row['id'].'">
					<img src="img/mov/'.$row['url_pic'].'" '.$parteInicio.$parteMedio.$defRoute.$parteFinal.'class="img-fluid margenbot" alt="img/mov/1995.jpg" onmouseOver="JSFX.zoomIn(this) onmouseOut="JSFX.zoomOut(this)">
				</a>
			<div class="crop">
						<a href="https://repelis.me/05/star-wars-episodio-ix-el-ascenso-de-skywalker-2019/" class="info-title one-line">
							<h6 class="crop">'.$row['title'].'</h6>
						</a>
					</div>
				</div>
		';
	}
	
	//Ahora se hace una consulta que tiene que devolver el mismo numero de rows que la consulta anterior
	$sql = "SELECT COUNT(id) AS total FROM movie"; 
	$result = $connection->query($sql);
	$row = $result->fetch_assoc();
	$total_pages = ceil($row["total"] / $results_per_page);
	
	//AQUI TENGO LOS HREF PARA CADA PAGINA tengo que pasar $page(current page) y $total_pages
	echo ImprimePageNavigation($getpage,$total_pages,$getorderby);

}
function ImprimeSearchv2($getpage,$search,$getorderby){
	$searchgeteado = $search;
	//MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
	
	//onerror="this.onerror=null; this.src='Default.jpg'"
	$defRoute	 = "'img/mov/default.jpg'";
	$parteMedio	 = "this.onerror=null; this.src=";
	$parteInicio = 'onerror="';
	$parteFinal  = '"';	
	//$parteInicio.$parteMedio.$defRoute.$parteFinal
	
    //connect to mysqli database (Host/Username/Password)
   $connection = mysqli_connect($db_host, $db_username, $db_password);
	$db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
	if($connection ->connect_error){
		die("Conexion fallida: " . $connection->connect_error);
	}//insert query goes here
	$results_per_page = 24;
	if(isset($getpage)){
		$page = $getpage;
	}else {
		$page = 1;
	}
	$start_form = ($page-1) * $results_per_page;
	//Consulta para obtener todas las peliculas Limitando su echo a $resuls per page
	if($getorderby == 'mierda'){
		//echo 'caso a';
		//echo $getorderby;
		$sql = "SELECT id,title, url_pic, url_imdb FROM movie WHERE title LIKE '%$search%' LIMIT $start_form,$results_per_page";
	
	}else if($getorderby == 'name'){
		//echo 'caso b';
		//echo $getorderby;
		$sql = "SELECT id,title,url_pic FROM movie WHERE title LIKE '%$search%' ORDER BY title LIMIT $start_form,$results_per_page";
	}else{
		//echo 'caso c';
		//echo $getorderby;
		$sql = "SELECT id,avg,title,url_pic FROM (SELECT id_movie AS id,AVG(score)as avg,(SELECT title FROM movie WHERE id=id_movie AND title LIKE '%an%') AS title,(SELECT url_pic FROM movie WHERE id=id_movie AND title LIKE '%an%') AS url_pic FROM user_score GROUP BY id_movie) AS t1 WHERE title IS NOT NULL ORDER BY avg DESC LIMIT $start_form,$results_per_page";
		
	}
	//Ejecucin de la consulta anterior
	$rs_result = $connection->query($sql);
	//Imprime los resultados (limitado a $result_per_page)
	while($row = $rs_result->fetch_assoc()){
		echo ' 
		<div class="col-md-2">
			<a href="details.php?id='.$row['id'].'">
					<img src="img/mov/'.$row['url_pic'].'" '.$parteInicio.$parteMedio.$defRoute.$parteFinal.'class="img-fluid margenbot" alt="img/mov/1995.jpg">
				</a>
			<div class="crop">
						<a href="https://repelis.me/05/star-wars-episodio-ix-el-ascenso-de-skywalker-2019/" class="info-title one-line">
							<h6 class="crop">'.$row['title'].'</h6>
						</a>
					</div>
				</div>
		';
	}
	
	//Ahora se hace una consulta que tiene que devolver el mismo numero de rows que la consulta anterior
	$sql = "SELECT COUNT(id) AS total FROM movie WHERE title LIKE '%$search%'"; 
	$result = $connection->query($sql);
	$row = $result->fetch_assoc();
	$total_pages = ceil($row["total"] / $results_per_page);
	
	//AQUI TENGO LOS HREF PARA CADA PAGINA tengo que pasar $page(current page) y $total_pages
	echo ImprimePageNavigationSearch($getpage,$total_pages,$searchgeteado,$getorderby);

}
function ImprimePageNavigation($getCurrentpage,$numPaginas,$orden){
	if(isset($getCurrentpage)){
		$page = $getCurrentpage;
	}else {
		$page = 1;
	}
	$previous = $getCurrentpage -1;
	$next	  = $getCurrentpage +1;
	if($numPaginas>1){
		if(!isset($page) || $page==1){
			echo '
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="index.php?orderby='.$orden.'&page=2"><strong>Next</strong></a></li>
			</ul>
			</div>';
		}else if($page == $numPaginas){
			echo'
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="index.php?orderby='.$orden.'&page='.$previous.'"><strong>Previous</strong></a></li>
			</ul>
			</div>';
		}else {
			echo'
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="index.php?orderby='.$orden.'&page='.$previous.'"><strong>Previous</strong></a></li>
			  <li class="page-item"><a class="page-link" href="index.php?orderby='.$orden.'&page='.$next.'"><strong>Next</strong></a></li>
			</ul>
			</div>';
		}
	}
}
//funcion similar al search pero con genre
function ImprimeSearchv2genre($getpage,$genre,$getorderby){
	//MySQLi information
    $db_host     = "localhost";
    $db_username = "root";
    $db_password = "";
	
	//onerror="this.onerror=null; this.src='Default.jpg'"
	$defRoute	 = "'img/mov/default.jpg'";
	$parteMedio	 = "this.onerror=null; this.src=";
	$parteInicio = 'onerror="';
	$parteFinal  = '"';	
	//$parteInicio.$parteMedio.$defRoute.$parteFinal
	
    //connect to mysqli database (Host/Username/Password)
   $connection = mysqli_connect($db_host, $db_username, $db_password);
	$db = mysqli_select_db($connection, "ai21") or die("Error " . mysqli_error());
	if($connection ->connect_error){
		die("Conexion fallida: " . $connection->connect_error);
	}//insert query goes here
	$results_per_page = 24;
	if(isset($getpage)){
		$page = $getpage;
	}else {
		$page = 1;
	}
	$start_form = ($page-1) * $results_per_page;
	//Consulta para obtener todas las peliculas Limitando su echo a $resuls per page
	if($getorderby == 'mierda'){
		echo 'caso a';
		echo $getorderby;
		$sql = "SELECT * FROM movie WHERE id IN (SELECT movie_id FROM moviegenre WHERE genre=$genre) LIMIT $start_form,$results_per_page";
	
	}else if($getorderby == 'name'){
		echo 'caso b';
		echo $getorderby;
		$sql = "SELECT * FROM movie WHERE id IN (SELECT movie_id FROM moviegenre WHERE genre=$genre) ORDER BY title LIMIT $start_form,$results_per_page";
	}else{
		echo 'caso c';
		echo $getorderby;
		$sql = "SELECT id,title,url_pic,avg FROM (SELECT id_movie AS id,AVG(score)as avg,(SELECT title FROM movie WHERE id=id_movie) AS title,(SELECT url_pic FROM movie WHERE id=id_movie) AS url_pic FROM user_score GROUP BY id_movie) as t1 WHERE id IN(SELECT movie_id FROM moviegenre WHERE genre=$genre) ORDER BY avg DESC LIMIT $start_form,$results_per_page";
		
	}
	//Ejecucin de la consulta anterior
	$rs_result = $connection->query($sql);
	//Imprime los resultados (limitado a $result_per_page)
	while($row = $rs_result->fetch_assoc()){
		echo ' 
		<div class="col-md-2">
			<a href="details.php?id='.$row['id'].'">
					<img src="img/mov/'.$row['url_pic'].'" '.$parteInicio.$parteMedio.$defRoute.$parteFinal.'class="img-fluid margenbot" alt="img/mov/1995.jpg">
				</a>
			<div class="crop">
						<a href="https://repelis.me/05/star-wars-episodio-ix-el-ascenso-de-skywalker-2019/" class="info-title one-line">
							<h6 class="crop">'.$row['title'].'</h6>
						</a>
					</div>
				</div>
		';
	}
	
	//Ahora se hace una consulta que tiene que devolver el mismo numero de rows que la consulta anterior
	$sql = "SELECT COUNT(id) AS total FROM movie WHERE id IN (SELECT movie_id FROM moviegenre WHERE genre=$genre)"; 
	$result = $connection->query($sql);
	$row = $result->fetch_assoc();
	$total_pages = ceil($row["total"] / $results_per_page);
	
	//AQUI TENGO LOS HREF PARA CADA PAGINA tengo que pasar $page(current page) y $total_pages
	echo ImprimePageNavigationGenre($getpage,$total_pages,$genre);

}
function ImprimePageNavigationGenre($getCurrentpage,$numPaginas,$genre){
	if(isset($getCurrentpage)){
		$page = $getCurrentpage;
	}else {
		$page = 1;
	}
	$previous = $getCurrentpage -1;
	$next	  = $getCurrentpage +1;
	if($numPaginas>1){
		if(!isset($page) || $page==1){
			echo '
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="genre.php?genre='.$genre.'&page=2"><strong>Next</strong></a></li>
			</ul>
			</div>';
		}else if($page == $numPaginas){
			echo'
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="genre.php?genre='.$genre.'&page='.$previous.'"><strong>Previous</strong></a></li>
			</ul>
			</div>';
		}else {
			echo'
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="genre.php?genre='.$genre.'&page='.$previous.'"><strong>Previous</strong></a></li>
			  <li class="page-item"><a class="page-link" href="genre.php?genre='.$genre.'&page='.$next.'"><strong>Next</strong></a></li>
			</ul>
			</div>';
		}
	}
}
function ImprimePageNavigationSearch($getCurrentpage,$numPaginas,$search,$orden){
	if(isset($getCurrentpage)){
		$page = $getCurrentpage;
	}else {
		$page = 1;
	}
	$previous = $getCurrentpage -1;
	$next	  = $getCurrentpage +1;
	if($numPaginas>1){
		if(!isset($page) || $page==1){
			echo '
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="search.php?search='.$search.'&page=2"><strong>Next</strong></a></li>
			</ul>
			</div>';
		}else if($page == $numPaginas){
			echo'
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="search.php?search='.$search.'&page='.$previous.'"><strong>Previous</strong></a></li>
			</ul>
			</div>';
		}else {
			echo'
			<div class="container">
			<ul class="pagination">
			  <li class="page-item"><a class="page-link" href="search.php?search='.$search.'&page='.$previous.'"><strong>Previous</strong></a></li>
			  <li class="page-item"><a class="page-link" href="search.php?search='.$search.'&page='.$next.'"><strong>Next</strong></a></li>
			</ul>
			</div>';
		}
	}
}
?>