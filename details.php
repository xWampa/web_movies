<!DOCTYPE html>
<?php include('funciones_generales_php.php');?>

<?php include_once 'user.php';?>
<?php include_once 'user_session.php';?>

<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minumum-scale=1.0">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/custom.css">
		<title>Pewelilucas</title>	
	</head>
	<!--#################################################################################################################################################-->
<body>
		<?php  
	session_start(); //Para conseguir la sesion de otras paginas
	
	if(isset($_SESSION['user'])) {
  		//echo "Your session is running " . $_SESSION['user'];
		echo ImprimirMenuA();
	}else{
		echo ImprimirMenuNA();
	}	
	?>
		<div class="container">
    <div class="row my-2">
        <div class="col-lg-8 order-lg-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Detalles</a>
                </li>
                <li class="nav-item">
                    <a href="" data-target="#messages" data-toggle="tab" class="nav-link">Opinions</a>
                </li>
				<?php
				if(isset($_SESSION['user'])){
					echo '
					<li class="nav-item">
                    <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Edit rating</a>
                </li>
					';
				}
				?>
                
            </ul>
            <div class="tab-content py-4">
                <div class="tab-pane active" id="profile">
                    <div class="row">
                        <div class="col-md-6">
                            
							<?php echo ImprimeDetalles($_GET['id'])?>
                        </div>
						<div class="col-md-6">
                            <h6>Genres</h6>
							<?php echo ImprimeGenres($_GET['id'])?>
                            <hr>
                        </div>
                       
                        
                    </div>
                    <!--/row-->
                </div>
				<!--/MAYBE TUS ULTIMOS COMENTARIO A PELICULAS-->
                <div class="tab-pane" id="messages">
						<div class="alert alert-info alert-dismissable">
							<a class="panel-close close" data-dismiss="alert">×</a> Hey there! We <strong>encourage</strong> you to write about this film if you have seen it.
						</div>
                    <table class="table table-hover table-striped">
                        <tbody>                                    
                            <?php echo ImprimeOpiniones($_GET['id']);?>
                        </tbody> 
						
                    </table>
					
					
					<?php
					if(isset($_SESSION['user'])){
						echo'
					<h5 class="h5 mb-3 font-weight-normal"><strong>Write your thoughts about this film!</strong></h5>
					
					 <form role="form" action="maneja_comentario.php?id='.$_GET['id'].'" method="POST" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-lg-9">
                                <textarea name="comentario" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea><!--/NOMBER-->
                            </div>
                        </div>
                        
							  <input type="hidden" name="paradb1" value="'.$_SESSION['user'].'">
						  
						
						<br>
						<button name="submit" class="btn btn-lg btn-primary btn-block" type="submit">Send comment</button>
							
                    </form>';
					}
					?>
					<?php
					//if(isset($_SESSION['user'])){
					
						//if(isset($_POST['submit'])){
							//echo $_GET['id'];
							//echo $_POST['text'];
							//echo $_SESSION['user'];
						 	//echo AnadeComentario($_GET['id'],$_POST['text'],$_SESSION['user']);
						//}
					//}
					?>
                </div>
				<?php
				if(isset($_SESSION['user'])){
					echo '
					
                <div class="tab-pane" id="edit">
					'.
                                  ImprimeScore($_SESSION['user'],$_GET['id']).'
                            
					<h6>Change rating to:</h6>
                            
                    <form role="form" action="maneja_rating.php?id='.$_GET['id'].'" method="POST" enctype="multipart/form-data">
                       
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">New rating:</label>
                            <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="1" required="" autofocus="">
				  <label class="form-check-label" for="inlineRadio2">1</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="2">
				  <label class="form-check-label" for="inlineRadio1">2</label>
                            </div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="3">
				  <label class="form-check-label" for="inlineRadio1">3</label>
                            </div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="4">
				  <label class="form-check-label" for="inlineRadio1">4</label>
                            </div>
							<div class="form-check form-check-inline">
								<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="5">
				  <label class="form-check-label" for="inlineRadio1">5</label>
                            </div>
                        </div>
                      
						
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-9">
								<input type="hidden" name="paradb1" value="'.$_SESSION['user'].'">
                                <input type="reset" class="btn btn-secondary" value="Cancel">
                                <input type="submit" class="btn btn-primary" value="Save Changes">
								
                            </div>
                        </div>
                    </form>
						
						</label>
                </div>';
					  }
				?>
			<!--HASTA AQUI el EDIT-->
            </div>
        </div>
        <div class="col-lg-4 order-lg-1 text-center">
			<?php echo ImprimeImagen($_GET['id']);?>
            <!--img src="//placehold.it/150" class="mx-auto img-fluid img-circle d-block" alt="avatar"-->
            
        </div>
    </div>
</div>
<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/popper.min.js"></script>
		</body>
</html>