<!DOCTYPE html>
<?php include_once 'funciones_generales_php.php';?>
<?php include_once 'user.php';?>
<?php include_once 'user_session.php';?>
<?php echo TodaLaPescaDeSesion();?>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minumum-scale=1.0">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap.css">
		<link rel="stylesheet" href="css/custom.css">
		
		<title>Login</title>	
	</head>
		<body class="text-center" style="width: 300">

			<form class="form-signin" method="post" action="login.php">
				<a href="index.php">
			  		<img class="mb-4" src="img/LogoTLP.png" alt="" width="72" height="72" >
				</a>	<br>
				<p class="text-danger">
				<?php
					if(isset($errorLogin)){
						echo $errorLogin;
					}
				?>
					</p>
			  <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
			  <label for="inputEmail" class="sr-only">Email address</label>
			  <input type="name" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="" name="username">
			  <label for="inputPassword" class="sr-only">Password</label>
			  <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="" name="password">
			  <div class="checkbox mb-3">
				<label>
				  <input type="checkbox" value="remember-me"> Remember me
				</label>
			  </div>
			  <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
			  <a class="mt-5 mb-3 text-muted" href="register.php">Are you new? Register now!</a>
			</form>

			<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/popper.min.js"></script>
		</body>
</html>